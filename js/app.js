const begin = () => {
  const $fabContainer = $('#fab');

  // init modal
  $('.modal').modal();
  // main
  const insertAction = event => {
    switch (true) {
    case event.currentTarget.id === 'add-message':
      MSG.newForm();
      break;
    case event.currentTarget.id === 'add-image':
      IMG.newForm();
      break;
    case event.currentTarget.id === 'add-event':
      EVENT.newForm();
      break;
    case event.currentTarget.id === 'add-media':
      MEDIA.newForm();
      break;
    }
  };

  $fabContainer.on('click', 'li', insertAction);
};

$(document).ready(begin);
